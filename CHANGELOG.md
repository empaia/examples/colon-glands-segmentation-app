# Changelog

## 0.6.1

* Fixed error in output references to solve output validation error
* changed pixelmap level definition

## 0.6.0

* Added pixelmaps as outputs

## 0.5.8
* Add a class for explanations


## 0.5.7
* Normalize prediction score
* Fix wrong image version tag (from master-dev to actual version)

## 0.5.6
* Change prediction score posting to include class name and not only index
* Limit image push to latest and current version

## 0.5.5
* Fix wrong proxy configuration for building docker images in CI/CD pipeline (charite runner specific)
* Fix pushing docker image with tag latest too
* Improve docker image build time by using "latest" image (=image with tag "latest") as cache

## 0.5.4
* Fix missing square for area transformation factor

## 0.5.3
* Decrease Dockerfile size
* Change image build tool from kaniko to docker

## 0.5.2
* Fix dictionary constants are not copied and used correctly

## 0.5.1
* Update ReadMe to reflect the newest classification model

## 0.5.0
* Create explanations for the classification model

## 0.4.0
* Use new classification model
* classifying glands as: normal, serrated, adenocarinoma, adenoma

## 0.3.9
* fix error when posting large ROIs

## 0.3.8
* add prediction score per class for each polygon (before merging)
* see version 0.3.3 for reference
* improve EAD output variable naming

## 0.3.7
* fix softmax applied to wrong dimension

## 0.3.6
* fix docker image scan timeout

## 0.3.5
* improve gitlab pipeline (specifically when quick tests fail fast)

## 0.3.4
* add segmented glands' area

## 0.3.3
* add prediction scores per class for each merged polygon

## 0.3.2
* add more tests on segmentation and classification merging

## 0.3.1
* fix image build

## 0.3.0
* introduce CI/CD pipeline

## 0.2.10
* Fix issue of having more than 3 output classes

## 0.2.9
* allow posting of many polygons (=large ROI, high resolution)

## 0.2.8
* increase segmentation explanation rectangle resolution

## 0.2.7
* fix explanation image rotation around axis (left-up to right-down)

## 0.2.6
* set explanation iteration to 20

## 0.2.5
* fix wrong id reference between explanation values and segmentation (!) polygons

## 0.2.4
* restructure utilities

## 0.2.3
* add model weights from share folder

## 0.2.2
* fix wrong resolution usage

## 0.2.1
* remove unnecessary requirements.txt

## 0.2.0
* introduce the generation of explanations

## 0.1.1

* Fix: Move models to CUDA if available

## 0.1.0

* Introduce a structured version of the colon glands segmentation app for the EMPAIA project
* With following functionality:
  * Colon glands segmentation
  * Segmentation classification
