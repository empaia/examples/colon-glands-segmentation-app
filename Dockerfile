FROM registry.gitlab.com/empaia/integration/ci-docker-images/test-runner:0.2.0@sha256:81523e4a90baa876e4f6411c66566ff246974457c4034d5924bd6c63de76e2af AS builder

WORKDIR /tmp
# Download model
RUN apt-get install git-lfs -y --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN git lfs install && git clone https://huggingface.co/nvidia/segformer-b3-finetuned-ade-512-512

# Download weights
RUN curl -OJ https://tubcloud.tu-berlin.de/s/kcqZDs4LeQjsnsE/download && \
    unzip weights

COPY . /tmp/
RUN poetry build && poetry export --without-hashes -f requirements.txt > requirements.txt

FROM registry.gitlab.com/empaia/integration/ci-docker-images/python-base:0.2.1@sha256:b7e4101980a21d2c5ac69a660ed3d3eb960d2993501ec256f1bd2e7b7c9d40d1
USER root
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install\
    git\
    libgl1\
    libgl1-mesa-glx \ 
    libglib2.0-0 -y \
    --no-install-recommends && apt-get clean && rm -rf /var/lib/apt/lists/*

USER appuser
WORKDIR /src
COPY --from=builder /tmp/requirements.txt /artifacts
RUN pip3 install -r /artifacts/requirements.txt --no-cache-dir
COPY --from=builder /tmp/dist/*.whl /artifacts
RUN pip3 install /artifacts/*.whl --no-cache-dir

COPY . .
COPY --from=builder /tmp/weights/ app/weights/
COPY --from=builder /tmp/segformer-b3-finetuned-ade-512-512 segformer-b3-finetuned-ade-512-512

ENTRYPOINT python -u app/main.py
