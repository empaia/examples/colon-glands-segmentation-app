## EMPAIA Colon Glands Segmentation App :fire:

A demonstrator application for the [EMPAIA Ecosystem for Digital Pathology](https://www.empaia.org/), as featured in [Romberg, D., Strohmenger, K., Jansen, C., Küster, T., Weiss, N., Geißler, C., ... & Homeyer, A. (2021). EMPAIA App Interface: An open and vendor-neutral interface for AI applications in pathology. Computer Methods and Programs in Biomedicine, 106596.](https://doi.org/10.1016/j.cmpb.2021.106596)

This app wraps a model developed and trained by Mohamed Amine Dhiab and Eric Kolibacz (GT-ARC). This model is capable of segmenting and classifying colon glands.

### Requirements :pushpin:

- [Docker Engine](https://docs.docker.com/engine/install/)
- [empaia-app-test-suite >= 3.5.0](https://pypi.org/project/empaia-app-test-suite/) (EATS)

> Refer to the [developer documentation](https://developer.empaia.org/app_developer_docs/draft-3/#/app_test_suite?id=installation) for a step-by-step guide to installing EATS and running the app using it.

- Up to 10 GB storage space for application docker image (additional storage space is required for the EATS services)
- An H&E colon glands WSI in the data directory mounted by the EATS with the name `colon.tiff` (can be changed in `./inputs/my_wsi.json`) - how to obtain an example file can be found [further down](#colon-glands-example-image-for-development)
- Optional: CUDA-enabled GPU

### App Workflow

|                                                                  <div align="center">Step 1</div>                                                                   |                                                                                                                      <div align="center">Step 2</div>                                                                                                                       |                                                                                                                                     <div align="center">Step 3</div>                                                                                                                                     |
| :-----------------------------------------------------------------------------------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| <p align="center"><img src="./screenshots/workflow_1.png" width="200" ><br/><em>Preselect a region from the WSI that seems interesting for you to analyze.</em></p> | <p align="center"> <img src="./screenshots/workflow_2.png" width="200" ><br/><em>Draw a rectangular region with the select tool which will be considered as a Region of Interest (RoI) and will then be divided into patches and fed subsequently to the AI model.</em></p> | <p align="center"> <img src="./screenshots/workflow_3.png" width="200" ><br/><em>When the model inference has finished, the polygons defining the segmented glands inside the rectangular RoI will be shown as a result on the UI (the color of the polygon determines the class of the gland).</em></p> |

### Launching the app with the EMPAIA App Test Suite (EATS) :page_with_curl:

1. First, you need to provide a **Whole Slide Image** of colon tissue and remember the path to it (which will be used when running `eats`). How to find an example WSI is described [further down](#colon-glands-example-image-for-development).

2. Add the data folder path to your WSI mount points file. For example, if your WSI is located in `/home/user/data/colon.tiff` and you want to mount it to `/data`, add this configuration to your `wsi-mount-points.json`:

```json
{
    "/home/user/data/": "/data"
}
```

3. Start `eats` with the WSI mount point:

```bash
# withou GPU support
eats service up /path/to/wsi-mount-points.json
# with GPU support
eats service up /path/to/wsi-mount-points.json --gpu-driver nvidia  
```

4. Clone the Backend App

```bash
git clone https://gitlab.com/empaia/examples/colon-glands-segmentation-app.git
cd colon-glands-segmentation-app/
```

5. In order to register the AI App as well your Whole Slide Image run the script file

```bash
./script.sh
```

6. Open the _EMPAIA Workbench-Client_ in your browser through `http://localhost:8888/wbc3/`

7. Select the Case from the **CASES** Tab:

<div align="center">
<img src="./screenshots/cases.png" alt="cases" width="600"/>
</div>

8. Add the App using `+Add new App` from the **APPS** Tab:

<div align="center">
<img src="./screenshots/add_app.png" alt="apps" width="600"/>
</div>

9. Start the App by selecting it and then clicking on `Start` as the following:

<div align="center">
<img src="./screenshots/start_app.png" alt="apps" width="600"/>
</div>

10.  Select your slide from the `SLIDES` panel on the right side of the screen:

<div align="center">
<img src="./screenshots/select_slide.png" alt="apps" width="800"/>
</div>

11. Select a region of interest by drawing a rectangular annotation on the WSI using the rectangle cursor:

<div align="center">
<img src="./screenshots/select_roi.png" alt="apps" width="400"/>
</div>

12. After waiting for the job to finish and the annotations to be retrieved, you will get a result as the following:

<div align="center">
<img src="./screenshots/result.png" alt="results" width="800"/><br/>
<em>The result displays a set of polygons (red & blue) inside the rectangular Region of Interested (yellow) selected by the pathologist. Each polygon corresponds to a detected gland inside that region, each polygon's color correspond to the class of the gland. Red for normal glands, blue for serrated, green for adenocarinoma and pink for adenoma. </em>
</div>

<br/>

### Datasets

The Data used to train the segmentation model (SegFormer) and the classification model (EfficientNet-b2) was extracted from:

-   <a href="https://www.kaggle.com/competitions/hubmap-organ-segmentation">HuBMAP + HPA - Hacking the Human Body, a Kaggle competition.</a>

-   <a href="https://warwick.ac.uk/fac/cross_fac/tia/data/glascontest">GlaS@MICCAI'2015: Gland Segmentation Challenge Contest</a>

-   <a href="https://bupt-ai-cz.github.io/HSA-NRL/">Chaoyang Dataset</a>

If you intend to publish research work that uses the second dataset or the result of our work, you must cite the following papers:

<br/>

<p style="background-color: #f2f2f2">
K. Sirinukunwattana, J. P. W. Pluim, H. Chen, X Qi, P. Heng, Y. Guo, L. Wang, B. J. Matuszewski, E. Bruni, U. Sanchez, A. Böhm, O. Ronneberger, B. Ben Cheikh, D. Racoceanu, P. Kainz, M. Pfeiffer, M. Urschler, D. R. J. Snead, N. M. Rajpoot, "Gland Segmentation in Colon Histology Images: The GlaS Challenge Contest"</p>

<br/>

<p style="background-color: #f2f2f2">K. Sirinukunwattana, D.R.J. Snead, N.M. Rajpoot, "A Stochastic Polygons Model for Glandular Structures in Colon Histology Images," in IEEE Transactions on Medical Imaging, 2015 doi: 10.1109/TMI.2015.2433900</p>

<br/>

The Chaoyang Dataset is made freely available to academic and non-academic entities for non-commercial purposes such as academic research, teaching, scientific publications, or personal experimentation. Permission is granted to use the data given that you agree to their <a href="https://github.com/bupt-ai-cz/HSA-NRL#license">license</a>.

<br/>

#### Colon glands example image for development

For development with `eats`, an example WSI file is required. One possibility to access a valid file is the following:
1. Navigate to <a href="https://www.kaggle.com/competitions/hubmap-organ-segmentation">HuBMAP + HPA - Hacking the Human Body, a Kaggle competition</a>
2. Find the image ID of the training set which cell type is *largeintestine* (on Kaggle you can filter for the keyword), e.g., **11890**
3. Download the file, rename it to `colon.tiff` and place it in the data folder defined in the `wsi-mount-points.json`
4. Install `vips` (on Ubuntu: `apt install libvips-tools`)
5. Run `vips tiffsave colon.tiff colon.tiff --pyramid --tile --tile-width 256 --tile-height 256 --bigtiff --compression jpeg --Q 90 --xres=4366.81222707 --yres=4366.81222707` - now the metadata of the file is valid

### Remarks

:warning: This source code and app is for training and research purposes only. :warning:

:red_circle: You must not use it for medical diagnosis. :red_circle:

(c) GT-ARC 2022

If you need to use this work, please refer to <a href="mailto:mohamedamine.dhiab@gt-arc.com">mohamedamine.dhiab@gt-arc.com</a>.
