"""Module containing all constants"""
import logging
import os

RESOLUTION = 2**4  # needs to be a power of 2, e.g. 2**2, 2**3, 2**4, ...
# minimum 2**2 = 4

APP_API = os.environ["EMPAIA_APP_API"]
JOB_ID = os.environ["EMPAIA_JOB_ID"]
TOKEN = os.environ["EMPAIA_TOKEN"]
# you can change this variable according to the model weights file name
MODEL_WEIGHTS = "model_densenet_1.pth"
SEGFORMER_WEIGTHS = "./app/weights/segFormer.pth"
CLASSIFICATION_WEIGHTS = "./app/weights/histoEncoder_small_classifier_HE_FROZEN.pth"
SEGMENTATION_THRESHOLD = 0.15

# explanation constants
EXPLANATION_ITERATIONS = 2

# posting constants
OUTPUT_POSTING_BATCH_SIZE = 10000

logger = logging.getLogger("app")
logging.basicConfig(format="%(asctime)s - %(message)s", level=logging.INFO)
