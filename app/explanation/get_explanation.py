"""Module which enables generating explanations for a segmentation model given an image."""
from typing import Any, Dict, Iterable, List, Tuple, Union

import maxi
import numpy as np
import torch
from maxi import ExplanationGenerator
from maxi.data.data_types import EntityRect
from maxi.lib.inference import InferenceWrapper
from maxi.lib.inference.processing.selective_region_processor import Torch_SelectiveRegionProcessor
from maxi.lib.inference.quantizer.confidence_method import TorchBinaryConfidenceMethod
from maxi.lib.inference.quantizer.identity_quantizer import IdentityMethod
from shapely.geometry import Polygon

# pylint: disable=C0103,R0914

loss_class = maxi.loss.Torch_CEMLoss
optimizer_class = maxi.optimizer.AdaExpGradPOptimizer
gradient_class = maxi.gradient.Torch_Gradient

loss_kwargs = {"mode": "PPSMOOTH", "c": 1, "gamma": 0}
optimizer_kwargs = {"l1": 0.1, "l2": 0.1, "channels_first": False}
gradient_kwargs = {"mu": 0.01, "num_samples": 125, "channels_first": False, "batch_mode": False}


def get_classification_explanation(
    image: np.ndarray,
    classification_model: torch.nn.Module,
    region_of_interest: EntityRect,
    iterations: int = 20,
    device: str = "cpu",
) -> np.ndarray:
    """Get the explanation for a given image and classification model.
    Explanation algorithm is based on a modified version of the Maxi library. Here, the gradient
    is not estimated but directly accessed. This is done to increase the speed of the explanation
    generation.

    Args:
        image (np.ndarray): image to explain
        classification_model (torch.nn.Module): classification model
        region_of_interest (EntityRect): region to explain
        iterations (int, optional): how often to optimize the explanations. Defaults to 20.
        device (str, optional): Computation device. Defaults to "cpu".

    Returns:
        np.ndarray: model explanation for the given region of interest
    """
    image = np.moveaxis(image, 0, -1)
    classification_model.train()
    processor = Torch_SelectiveRegionProcessor(image, region_of_interest, device=device)
    quantizer = IdentityMethod()
    infer_wrapper = InferenceWrapper(
        preprocess=processor.preprocess,
        inference_model=lambda x: classification_model(torch.unsqueeze(x, 0)),
        quantizer=quantizer,
    )
    meta_data = {
        "relative_rect_coords": region_of_interest,
        "original_entity": image[
            region_of_interest["x"] : region_of_interest["x"] + region_of_interest["w"],
            region_of_interest["y"] : region_of_interest["y"] + region_of_interest["h"],
        ],
    }

    # to run the explanation on the correct device
    loss_kwargs.update({"device": device})
    gradient_kwargs.update({"device": device})

    # init explanation generator
    cem = ExplanationGenerator(
        loss=loss_class,
        optimizer=optimizer_class,
        gradient=gradient_class,
        num_iter=iterations,
        loss_kwargs=loss_kwargs,
        optimizer_kwargs=optimizer_kwargs,
        gradient_kwargs=gradient_kwargs,
        save_freq=iterations // 2,
        verbose=False,
    )

    expls_and_meta: List[Tuple[Dict[str, np.ndarray], Dict[str, Any]]] = cem.run(
        meta_data["original_entity"], infer_wrapper, meta_data
    )

    return expls_and_meta[0][str(iterations)]  # get the last explanation iteration


def get_segmentation_explanation(
    image: np.ndarray,
    segmentation_model: torch.nn.Module,
    region_of_interest: EntityRect,
    iterations: int = 20,
    device: str = "cpu",
) -> np.ndarray:
    """Get the explanation for a given image and segmentation model.
    Explanation algorithm is based on a modified version of the Maxi library. Here, the gradient
    is not estimated but directly accessed. This is done to increase the speed of the explanation
    generation.

    Args:
        image (np.ndarray): image to explain
        segmentation_model (torch.nn.Module): segmentation model
        region_of_interest (EntityRect): region to explain
        iterations (int, optional): how often to optimize the explanations. Defaults to 20.
        device (str, optional): Computation device. Defaults to "cpu".

    Returns:
        np.ndarray: model explanation for the given region of interest
    """
    image = np.moveaxis(image, 0, -1)
    segmentation_model.train()
    processor = Torch_SelectiveRegionProcessor(image, region_of_interest, device=device)
    quantizer = TorchBinaryConfidenceMethod(
        preprocess=processor.postprocess, confidence_calculator=torch.mean, device=device
    )
    infer_wrapper = InferenceWrapper(
        preprocess=processor.preprocess,
        inference_model=lambda x: segmentation_model(torch.unsqueeze(x, 0)).squeeze(0),
        quantizer=quantizer,
    )
    meta_data = {
        "relative_rect_coords": region_of_interest,
        "original_entity": image[
            region_of_interest["x"] : region_of_interest["x"] + region_of_interest["w"],
            region_of_interest["y"] : region_of_interest["y"] + region_of_interest["h"],
        ],
    }

    # to run the explanation on the correct device
    loss_kwargs.update({"device": device})
    gradient_kwargs.update({"device": device})

    # init explanation generator
    cem = ExplanationGenerator(
        loss=loss_class,
        optimizer=optimizer_class,
        gradient=gradient_class,
        num_iter=iterations,
        loss_kwargs=loss_kwargs,
        optimizer_kwargs=optimizer_kwargs,
        gradient_kwargs=gradient_kwargs,
        save_freq=iterations // 2,
        verbose=False,
    )

    expls_and_meta: List[Tuple[Dict[str, np.ndarray], Dict[str, Any]]] = cem.run(
        meta_data["original_entity"], infer_wrapper, meta_data
    )

    return expls_and_meta[0][str(iterations)]  # get the last explanation iteration


def get_explanation_polygons(
    classification_explanation: np.ndarray,
    segmentation_explanation: np.ndarray,
    upper_left_explanation: Tuple[int, int],
    down_sampling_factor: int = 1,
    resolution: int = 1,
) -> List[List[Dict[str, Union[float, Polygon]]]]:
    """Get the coordinates and the value of the "superpixel" polygons from the explanation.
    A "superpixel" is a rectangle spanning multiple pixels. All "superpixels" together make up the original image.
    It is some sort of grouping pixels into low resolution space. The value is computed as the mean over all pixels
    and over all three color channels.

    Args:
        classification_explanation (np.ndarray): the given classfication explanation
        segmentation_explanation (np.ndarray): the given segmentation explanation
        upper_left_explanation (Tuple[int, int]): the coordinates of the upper right corner of the explanation
        down_sampling_factor (int, optional): the down sampling factor from zoom 0 to the image. Defaults to 1.
        resolution (int, optional): the resolution of the resulting polygons;
                                    1 meaning just one polygon, 2 meaning 4 resoluting polygons, etc.
                                    Defaults to 1.

    Returns:
        List[List[Dict[str, Union[float, Polygon]]]]: Mean value and
        coordinates of the "superpixel" polygons, with shape (rows, columns)
    """
    intervals_x = np.linspace(0, classification_explanation.shape[0], resolution + 1, dtype=int)
    intervals_y = np.linspace(0, classification_explanation.shape[1], resolution + 1, dtype=int)

    explanation_polygons = []
    for boundaries_x in pairwise(intervals_x):
        polygons = []
        for boundaries_y in pairwise(intervals_y):
            # TODO Fix this properly
            # this is ugly: explanation[y,x,:] -> but works for now
            # alternatively the function transform_boundaries_to_coordinates could be erroneous
            # but I did not find the mistake there
            cls_superpixel = classification_explanation[
                boundaries_y[0] : boundaries_y[1], boundaries_x[0] : boundaries_x[1], :
            ]
            cls_superpixel_mean = float(np.mean(cls_superpixel, axis=(0, 1, 2)))
            seg_superpixel = segmentation_explanation[
                boundaries_y[0] : boundaries_y[1], boundaries_x[0] : boundaries_x[1], :
            ]
            seg_superpixel_mean = float(np.mean(seg_superpixel, axis=(0, 1, 2)))
            polygon = transform_boundaries_to_polygon(
                [boundaries_x, boundaries_y], upper_left_explanation, down_sampling_factor
            )
            polygons.append(
                {
                    "classification_value": cls_superpixel_mean,
                    "segmentation_value": seg_superpixel_mean,
                    "polygon": polygon,
                }
            )
        explanation_polygons.append(polygons)

    return explanation_polygons


def transform_boundaries_to_polygon(
    boundaries: List[Tuple[int, int]],
    upper_left_explanation: Tuple[int, int],
    down_sampling_factor: int = 1,
) -> Polygon:
    """Transforms the boundaries (derived from the resolution) into the corresponding polygon of the
    "superpixel". Here, superpixel refers to the rectangle spanning multiple pixels. All "superpixels"
    make up the original image.

    Args:
        boundaries (List[Tuple[int, int]]): boundaries of the "superpixel" )/rectangle
        upper_left_explanation (Tuple[int, int]): coordinates of the upper left corner of the explanation
        down_sampling_factor (int): the down sampling factor from zoom 0 to the image. Defaults to 1.

    Returns:
        Polygon: polygon of the "superpixel"
    """
    boundary_x = boundaries[0]
    boundary_y = boundaries[1]
    upper_left = (boundary_x[0], boundary_y[0])
    lower_left = (boundary_x[0], boundary_y[1])
    lower_right = (boundary_x[1], boundary_y[1])
    upper_right = (boundary_x[1], boundary_y[0])
    rectangle = [upper_left, lower_left, lower_right, upper_right, upper_left]
    rectangle = (np.array(rectangle) + np.array([upper_left_explanation])) * down_sampling_factor
    return Polygon([tuple(coords.tolist()) for coords in rectangle])


def pairwise(iterable: Union[list, np.ndarray]) -> Iterable[Tuple[int, int]]:
    "s -> (s0, s1), (s2, s3), (s4, s5), ..."
    return zip(iterable, iterable[1:])
