"""Main module for the app."""
import asyncio
import math
import traceback
from collections import Counter, namedtuple

import numpy as np
import torch
from maxi.data.data_types import EntityRect
from shapely import Polygon

from app.constants import EXPLANATION_ITERATIONS, logger
from app.explanation.get_explanation import get_classification_explanation, get_segmentation_explanation
from app.models.segformer import get_classification_model, get_segmentation_model
from app.utilities.general import batch_splitter, get_patch_rectangles, load_config
from app.utilities.geometrical import (
    find_ploygons,
    limit_poly_to_roi,
    merge_interesecting_polygons,
    upsample_explanation,
)
from app.utilities.request import pixelmap_api
from app.utilities.request.get import get_input, get_wsi_tile
from app.utilities.request.post import post_output, post_segmentations
from app.utilities.request.put import put_failure, put_finalize

# pylint: disable=R0914,R0915,W0718

PixelmapPoint = namedtuple("PixelmapPoint", "x y")


async def main(config: dict) -> None:
    """Main function for the app.

    Args:
        config (dict): configuration for the app
    """
    logger.info(config)
    try:
        logger.info("Getting wsi meta data")
        wsi = get_input("my_wsi")

        logger.info("Determining the target zoom level")
        slide_nmpp = (wsi["pixel_size_nm"]["x"] + wsi["pixel_size_nm"]["y"]) / 2
        model_nmpp = config["pixel_size_nm"]
        if slide_nmpp > model_nmpp:
            msg = f"Pixel size of slide ({slide_nmpp} nm) is larger than pixel size of model ({model_nmpp} nm)"
            logger.error(msg)
            put_failure(msg)
            return
        downsample_factor = int(model_nmpp // slide_nmpp)  # [1, infinity[
        target_level = int(np.log2(downsample_factor))
        logger.info("Target level: %d", target_level)
        scale_factor = 1 - downsample_factor * slide_nmpp / float(model_nmpp)
        tile_size = int(np.ceil(config["image_size"] + config["image_size"] * scale_factor))

        logger.info("Preparing the segmentation model")
        device_name = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"
        model_segmenter, feature_extractor = get_segmentation_model(device_name, config["image_size"])
        logger.info("Preparing the classification model")
        model_classifier = get_classification_model(device_name)

        # TODO: check min/max size of scaled roi (should be at least the size of tile_size)
        logger.info("Getting roi")
        roi = get_input("my_roi")

        # scale ROI to the target level
        left_x, left_y = roi["upper_left"]
        width = roi["width"]
        height = roi["height"]

        left_x = int(left_x / downsample_factor)
        left_y = int(left_y / downsample_factor)
        width = math.ceil(width / downsample_factor)
        height = math.ceil(height / downsample_factor)

        # fit ROI to pixelmap grid making it bigger if necessary
        left_x_offset = left_x % tile_size
        left_y_offset = left_y % tile_size
        left_x -= left_x_offset
        left_y -= left_y_offset

        width += left_x_offset
        height += left_y_offset

        width += tile_size - width % tile_size
        height += tile_size - height % tile_size

        roi_scaled = {}
        roi_scaled["upper_left"] = [left_x, left_y]
        roi_scaled["width"] = width
        roi_scaled["height"] = height

        # TODO: use npp to extract patches from an appropriate wsi level by iterating over the wsi levels
        # https://developer.empaia.org/app_developer_docs/v2/#/specs/annotation_resolution?id
        # npp = (my_wsi.pixel_size_nm.x + my_wsi.pixel_size_nm.y) / 2
        # 0.25 micrometer/pixel scanned.

        logger.info("Posting the pixelmap")
        pixelmap = pixelmap_api.PIXELMAP.copy()
        pixelmap["name"] = f"{roi['name']} ROI explanation"
        pixelmap["reference_id"] = wsi["id"]
        pixelmap["tilesize"] = tile_size

        sorted_levels = wsi["levels"].copy()
        sorted_levels.sort(key=lambda x: x["downsample_factor"])

        # filter levels mit minimum tile size pixels
        sorted_levels = [
            level for level in sorted_levels if level["extent"]["x"] >= tile_size and level["extent"]["y"] >= tile_size
        ]

        # calculate min/max grid depending on rectangle and tile size
        x_roi, y_roi = roi_scaled["upper_left"]
        x_min = x_roi / tile_size
        x_max = (x_roi + roi_scaled["width"]) / tile_size
        y_min = y_roi / tile_size
        y_max = (y_roi + roi_scaled["height"]) / tile_size

        pm_levels = [
            {
                "slide_level": target_level,
                "position_min_x": int(x_min),
                "position_max_x": math.ceil(x_max) - 1,
                "position_min_y": int(y_min),
                "position_max_y": math.ceil(y_max) - 1,
            }
        ]

        pixelmap["levels"] = pm_levels
        pixelmap = post_output("explanation_map", pixelmap)
        logger.info("Pixelmap posted, id: %s", pixelmap["id"])

        logger.info("Getting patches within the scaled roi")
        patch_rectangles = get_patch_rectangles(roi_scaled, window=(tile_size, tile_size))
        logger.info("%f patches", len(patch_rectangles))

        polygons = []
        predictions = []
        # explanation_polygons = []

        # split rectangles into batches
        for batch in batch_splitter(patch_rectangles, config["batch_size"]):
            # fetch batch of tiles
            fetch_patch_tasks = [
                asyncio.create_task(get_wsi_tile(wsi, rect, target_level, feature_extractor)) for rect in batch
            ]
            tiles = await asyncio.gather(*fetch_patch_tasks)

            tiles_torch = torch.stack(tiles)
            tiles_torch = tiles_torch.to(device_name)
            logger.info("Running inference on %s tiles", tiles_torch.shape[0])
            model_segmenter.to(device_name)
            with torch.no_grad():
                masks = model_segmenter(tiles_torch)
                logits = model_classifier(tiles_torch)
                preds = torch.nn.Softmax(dim=1)(logits).detach().cpu().numpy()

            # generating explanations
            classification_explanations = []
            segmentation_explanations = []
            region_of_interest = EntityRect({"x": 0, "y": 0, "w": tiles_torch.shape[2], "h": tiles_torch.shape[3]})
            for tile in tiles:
                classification_explanations.append(
                    get_classification_explanation(
                        tile.cpu().detach().numpy(),
                        model_classifier,
                        region_of_interest,
                        iterations=EXPLANATION_ITERATIONS,
                        device=device_name,
                    )
                )
                segmentation_explanations.append(
                    get_segmentation_explanation(
                        tile.cpu().detach().numpy(),
                        model_segmenter,
                        region_of_interest,
                        iterations=EXPLANATION_ITERATIONS,
                        device=device_name,
                    )
                )

            logger.info("Collecting predictions")
            for pred, mask, classification_explanation, segmentation_explanation, rect in zip(
                preds, masks, classification_explanations, segmentation_explanations, batch
            ):
                upsampled_mask = torch.nn.functional.interpolate(
                    torch.Tensor(mask).unsqueeze(0), size=(tile_size, tile_size), mode="bilinear", align_corners=False
                )
                polys = find_ploygons(
                    upsampled_mask.squeeze().cpu().detach().numpy(), rect["upper_left"], downsample_factor
                )
                polygons.extend(polys)
                predictions.extend([pred] * len(polys))

                classification_explanation_upsampled = upsample_explanation(tile_size, classification_explanation)
                segmentation_explanation_upsampled = upsample_explanation(tile_size, segmentation_explanation)

                x_roi, y_roi = rect["upper_left"]
                if x_roi % tile_size == 0 and y_roi % tile_size == 0:
                    x_pm = int(x_roi / tile_size)
                    y_pm = int(y_roi / tile_size)

                    cls_map = np.mean(classification_explanation_upsampled, axis=2)
                    seg_map = np.mean(segmentation_explanation_upsampled, axis=2)

                    data = np.concatenate((cls_map, seg_map)).astype(dtype=pixelmap["element_type"]).tobytes()
                    pixelmap_api.put_tile(pixelmap["id"], target_level, x_pm, y_pm, data)

        if len(polygons) > 0:
            (
                segmentation_merged,
                segmentation_predictions,
                polygons_assigned,
                predictions_assigned,
            ) = merge_interesecting_polygons(polygons, predictions)
            segmentation_coordinates, polygon_values = limit_poly_to_roi(
                roi, segmentation_merged, [segmentation_predictions, polygons_assigned, predictions_assigned]
            )
            segmentation_predictions, polygons_assigned, predictions_assigned = polygon_values
            # segmentation area = area of polygon * NanometerPerPixel(nmpp) ** 2
            segmentation_area = [
                Polygon(coordinates).area * slide_nmpp**2 for coordinates in segmentation_coordinates
            ]
            areas_assigned = [
                [polygon.area * slide_nmpp**2 for polygon in polygons] for polygons in polygons_assigned
            ]
            segmentation_labels = [
                config["classes"][np.argmax(np.array(prediction))] for prediction in segmentation_predictions
            ]
            logger.info("Following label counts were found: %s", Counter(segmentation_labels))

            post_segmentations(
                wsi,
                roi,
                segmentation_coordinates,
                segmentation_labels,
                segmentation_predictions,
                segmentation_area,
                predictions_assigned,
                areas_assigned,
                config["classes"],
            )

        put_finalize()

    except Exception as error:
        logger.error(error)
        logger.info(traceback.format_exc())
        put_failure(str(error))


if __name__ == "__main__":
    asyncio.run(main(load_config("config.yaml")))
