"""The full model containing the feature extractor (from histoencoder) and the decoder (FPN)"""
import torch
from timm.models.xcit import xcit_medium_24_p16_224 as prostate_medium
from timm.models.xcit import xcit_small_12_p16_224 as prostate_small

NAME_TO_MODEL = {
    "prostate_small": prostate_small,
    "prostate_medium": prostate_medium,
}

# pylint: disable=C0103


class ClassificationModel(torch.nn.Module):
    """Colon-Gland classification model based on the pre-trained histoencoder (/XCit) model."""

    def __init__(
        self,
        hidden_layers: list = None,
        enocoder_size: str = "small",
        freeze_encoder: bool = False,
    ):
        super().__init__()
        if enocoder_size == "small":
            first_layer = 384
        elif enocoder_size == "medium":
            first_layer = 512
        else:
            raise ValueError(f"Unknown encoder size: {enocoder_size}, available sizes: small, medium")
        self.feature_encoder = NAME_TO_MODEL[f"prostate_{enocoder_size}"](num_classes=0)
        if freeze_encoder:
            for param in self.feature_encoder.parameters():
                param.requires_grad = False
        if hidden_layers is None:
            hidden_layers = []
        hidden_layers = [first_layer] + hidden_layers + [4]
        self.decode_head = torch.nn.Sequential(
            *[torch.nn.Linear(prev, nxt) for prev, nxt in zip(hidden_layers, hidden_layers[1:])]
        )

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass of the model.

        Args:
            x (torch.Tensor): model input

        Returns:
            torch.Tensor: model output
        """
        x = self.feature_encoder(x)
        x = self.decode_head(x)
        return x
