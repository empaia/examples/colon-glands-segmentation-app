"""This module contains the Segformer segementation and classification model."""
from typing import Tuple

import torch
from transformers import SegformerFeatureExtractor, SegformerForSemanticSegmentation

from app.constants import CLASSIFICATION_WEIGHTS, SEGFORMER_WEIGTHS, SEGMENTATION_THRESHOLD
from app.models.classification import ClassificationModel

# pylint: disable=C0103


def get_classification_model(device_name: str) -> torch.nn.Module:
    """Returns the classification model.

    Args:
        device_name (str): the pytorch-relevant device

    Returns:
        torch.nn.Module: the classification model
    """
    checkpoint = torch.load(CLASSIFICATION_WEIGHTS, map_location=device_name)
    model_classifier = ClassificationModel()
    model_classifier.load_state_dict(checkpoint["model_state_dict"])
    model_classifier.to(device_name)
    return model_classifier


def get_segmentation_model(device_name: str, tile_size: int) -> Tuple[torch.nn.Module, torch.nn.Module]:
    """Returns the segmentation model.

    Args:
        device_name (str): the pytorch-relevant device
        tile_size (int): the input tile size

    Returns:
        torch.nn.Module: the segmentation model
    """
    id2label_seg = {0: "OTHER", 1: "colon_gland"}

    feature_extractor = SegformerFeatureExtractor.from_pretrained("./segformer-b3-finetuned-ade-512-512/")
    feature_extractor.do_reduce_labels = True
    feature_extractor.size = int(tile_size)

    model_segmenter = SegformerForSemanticSegmentation.from_pretrained(
        "./segformer-b3-finetuned-ade-512-512/",
        return_dict=False,
        num_labels=len(id2label_seg.keys()),
        id2label=id2label_seg,
        label2id={v: k for k, v in id2label_seg.items()},
        ignore_mismatched_sizes=True,
    )
    model_segmenter.load_state_dict(torch.load(SEGFORMER_WEIGTHS, map_location=device_name))
    model_segmenter.to(device_name)

    model_extension = SegformerSegementationExtension(tile_size, SEGMENTATION_THRESHOLD)

    model = torch.nn.Sequential(model_segmenter, model_extension)
    model.eval()

    return model, feature_extractor


class SegformerSegementationExtension(torch.nn.Module):
    """This class is used to extend the Segformer model with a segmentation head."""

    def __init__(self, tile_size: int, threshold: float):
        super().__init__()
        self.interpolation = torch.nn.Upsample(
            size=(tile_size, tile_size),
            mode="bilinear",
            align_corners=False,
        )
        self.softmax = torch.nn.Softmax(dim=1)
        self.threshold = Threshold(threshold)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass of the model.

        Args:
            x (torch.Tensor): model input

        Returns:
            torch.Tensor: model output
        """
        x = self.interpolation(x[0])
        x = self.softmax(x)[:, 0, :, :]
        x = self.threshold(x)
        x = x.unsqueeze(1)
        return x


class Threshold(torch.nn.Module):
    """A class which "softly", differentiable thresholds a model output between 0 and 1."""

    def __init__(self, threshold: float):
        super().__init__()
        self.threshold = threshold
        self.alpha = 1024
        self.sigmoid = torch.nn.Sigmoid()

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """Forward pass of the model.

        Args:
            x (torch.Tensor): model input

        Returns:
            torch.Tensor: model output
        """
        x_thresholded = self.sigmoid(-self.alpha * (x - self.threshold))
        return x_thresholded
