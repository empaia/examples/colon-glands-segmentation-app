"""Some helper classes for testing."""
import numpy as np
import torch
from maxi.data.data_types import EntityRect

IMAGE = np.zeros((3, 256, 256))
IMAGE[:, 8:16, 8:16] = 1
UPPER_LEFT = (13, 203)
REGION_OF_INTEREST = EntityRect({"x": 4, "y": 4, "w": 16, "h": 16})
DOWN_SAMPLING_FACTOR = 3


class SimpleClassificationModel(torch.nn.Module):
    """Simple testing classification model which does not compute anything."""

    def __init__(self) -> None:
        super().__init__()

    def forward(self, network_input: torch.Tensor) -> torch.Tensor:
        """Forward pass of the model."""
        if network_input.shape[0] != 1 or network_input.shape[1] != 3:
            raise ValueError(f"Wrong network_input shape: {network_input.shape}, should be (1, 3, w, h)")
        output = torch.amax(network_input, dim=(2, 3))
        return output


class SimpleSegmentationModel(torch.nn.Module):
    """Simple testing segmentation model which does not compute anything."""

    def __init__(self) -> None:
        super().__init__()

    def forward(self, network_input: torch.Tensor) -> torch.Tensor:
        """Forward pass of the model."""
        if network_input.shape[0] != 1 or network_input.shape[1] != 3:
            raise ValueError(f"Wrong network_input shape: {network_input.shape}, should be (1, 3, w, h)")
        return torch.unsqueeze(network_input[:, 0, :, :], dim=1)


CLASSIFICATION_MODEL = SimpleClassificationModel()
SEGMENTATION_MODEL = SimpleSegmentationModel()
