"""A module to test the classification explanation module."""
import numpy as np
import pytest
from maxi.data.data_types import EntityRect

from app.explanation.get_explanation import get_classification_explanation
from app.tests import CLASSIFICATION_MODEL, IMAGE, REGION_OF_INTEREST

# pylint: disable=R0801


@pytest.mark.filterwarnings("ignore:.invalid value encountered in log")
def test_classification_explanation():
    """Test the classification explanation module."""
    explanation = get_classification_explanation(IMAGE, CLASSIFICATION_MODEL, REGION_OF_INTEREST)

    assert explanation.shape == (
        16,
        16,
        3,
    ), f"The explanation has the wrong shape {explanation.shape}, should be (16, 16, 3)"
    assert (explanation != 0).any(), "The explanation only contains zeros. This should not be the case."


def test_classification_explanation_simple():
    """Test the classification explanation module for a simple explanation."""
    region_of_interest = EntityRect({"x": 0, "y": 0, "w": 256, "h": 256})
    explanation = get_classification_explanation(IMAGE, CLASSIFICATION_MODEL, region_of_interest)

    is_img_max = IMAGE == np.max(IMAGE)
    is_img_max = np.moveaxis(is_img_max, 0, -1)

    assert (explanation[is_img_max] != 0).all()
    assert (explanation[~is_img_max] == 0).all()
