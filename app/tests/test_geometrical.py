"""Tests for geometrical module."""
import numpy as np
from shapely.geometry import MultiPolygon, Polygon

from app.utilities.geometrical import find_ploygons, merge_interesecting_polygons


def test_finding_polygons():
    """Tests finding polygons in a binary mask."""
    mask = np.zeros((10, 10))
    mask[0:2, 0:2] = 1
    expected_polygons = [
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square
    ]
    polygons = find_ploygons(mask, (0, 0), 1)
    assert MultiPolygon(polygons).equals(MultiPolygon(expected_polygons))


def test_finding_polygons_single_point():
    """Tests if a single point is not identified as a polygon."""
    mask = np.zeros((10, 10))
    mask[0, 0] = 1
    expected_polygons = []
    polygons = find_ploygons(mask, (0, 0), 1)
    assert MultiPolygon(polygons).equals(MultiPolygon(expected_polygons))


def test_merge_non_intersecting_polygons():
    """Tests if the function does not merge non-intersecting polygons."""
    polygons = [
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square
        Polygon([(2, 2), (2, 3), (3, 3), (3, 2)]),  # square far away
    ]
    predictions = [np.array([0.9, 0.1]), np.array([0.1, 0.9])]

    expected_labels = [0, 1]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [[polygons[0]], [polygons[1]]]
    assert predictions_assigned == [[predictions[0].tolist()], [predictions[1].tolist()]]


def test_merge_intersecting_polygons():
    """Tests if the function merges intersecting polygons."""
    polygons = [
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square
        Polygon([(0, 0.5), (0, 1.5), (1, 1.5), (1, 0.5)]),  # intersectping square
    ]
    predictions = [np.array([0.9, 0.1]), np.array([0.8, 0.2])]

    expected_polygons = [Polygon([(0, 0), (0, 1.5), (1, 1.5), (1, 0)])]
    expected_labels = [0]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(expected_polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [polygons]
    assert predictions_assigned == [[pred.tolist() for pred in predictions]]


def test_merge_intersecting_polygons_chain():
    """Tests if the function merges a chain of overlapping polygons correctly."""
    polygons = [
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # interscepting square 1
        Polygon([(0, 0.75), (0, 1.75), (1, 1.75), (1, 0.75)]),  # intersecpting square 0 and 2
        Polygon([(0, 1.5), (0, 2.5), (1, 2.5), (1, 1.5)]),  # intersecpting square 1
    ]
    predictions = [np.array([0.9, 0.1]), np.array([0.8, 0.2]), np.array([0.7, 0.3])]

    expected_polygons = [Polygon([(0, 0), (0, 2.5), (1, 2.5), (1, 0)])]
    expected_labels = [0]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(expected_polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [polygons]
    assert predictions_assigned == [[pred.tolist() for pred in predictions]]


def test_merge_intersecting_polygons_with_different_labels():
    """Tests if labels are assigned correctly when merging polygons."""
    polygons = [
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square with label 0
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square with label 1
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square with label 1
    ]
    predictions = [np.array([0.9, 0.1]), np.array([0.2, 0.8]), np.array([0.3, 0.7])]

    expected_polygons = [Polygon([(0, 0), (0, 1), (1, 1), (1, 0)])]
    expected_labels = [1]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(expected_polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [polygons]
    assert predictions_assigned == [[pred.tolist() for pred in predictions]]


def test_merge_intersecting_polygons_with_2different_labels():
    """Tests if labels are assigned correctly when merging polygons and labels are equal."""
    polygons = [
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square with label 0
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square with label 1
    ]
    predictions = [np.array([0.9, 0.1]), np.array([0.2, 0.8])]

    expected_polygons = [Polygon([(0, 0), (0, 1), (1, 1), (1, 0)])]
    expected_labels = [0]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(expected_polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [polygons]
    assert predictions_assigned == [[pred.tolist() for pred in predictions]]


def test_merge_intersecting_polygons_with_different_labels_and_size():
    """Tests if labels are determined correctly when merging polygons with different sizes."""
    polygons = [
        Polygon([(0, 0), (0, 1), (1, 1), (1, 0)]),  # square with label 0 and size 1
        Polygon([(0, 0), (0, 0.5), (1, 0.5), (1, 0)]),  # square with label 1 and size 0.5
    ]
    predictions = [np.array([0.9, 0.1]), np.array([0.2, 0.8])]

    expected_polygons = [Polygon([(0, 0), (0, 1), (1, 1), (1, 0)])]
    expected_labels = [0]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(expected_polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [polygons]
    assert predictions_assigned == [[pred.tolist() for pred in predictions]]


def test_merge_intersecting_polygons_with_different_labels_and_size2():
    """Tests if labels are determined correctly when merging polygons with different sizes.
    This example should be a little bit more difficult than the previous one."""
    polygons = [
        Polygon([(0, 0), (1, 0), (1, 2)]),  # triangle
        Polygon([(0.5, 0), (0.5, 1), (1, 2), (2, 2), (2, 0)]),  # half triangle (from above) with rectangle
        Polygon([(1, 0), (1, 2), (3, 2), (2, 1), (2, 0)]),  # rectangle (from above) with half triangle
    ]
    predictions = [np.array([0.501, 0.499]), np.array([0.8, 0.2]), np.array([0.2, 0.8])]

    expected_polygons = [Polygon([(0, 0), (1, 2), (3, 2), (2, 1), (2, 0)])]
    expected_labels = [0]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(expected_polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [polygons]
    assert predictions_assigned == [[pred.tolist() for pred in predictions]]


def test_finding_and_merging():
    """Tests the entire workflow: finding polygons, merging them and assigning labels."""
    mask = np.zeros((10, 10))
    mask[0:2, 0:2] = 1
    polygons1 = find_ploygons(mask, (0, 0), 1)
    mask = np.zeros((10, 10))
    mask[1:3, 0:2] = 1
    polygons2 = find_ploygons(mask, (0, 0), 1)
    polygons = polygons1 + polygons2
    predictions = [np.array([0.9, 0.1]), np.array([0.8, 0.1])]

    expected_polygons = [Polygon([(0, 0), (0, 2), (1, 2), (1, 0)])]
    expected_labels = [0]

    merged_polygon, merged_predictions, polygons_assigned, predictions_assigned = merge_interesecting_polygons(
        polygons, predictions
    )
    merged_labels = [np.argmax(np.array(prediction)) for prediction in merged_predictions]

    assert MultiPolygon(merged_polygon).equals(MultiPolygon(expected_polygons))
    assert merged_labels == expected_labels
    assert len(merged_polygon) == len(polygons_assigned)
    assert len(merged_predictions) == len(predictions_assigned)
    assert len(polygons_assigned) == len(predictions_assigned)
    assert polygons_assigned == [polygons]
    assert predictions_assigned == [[pred.tolist() for pred in predictions]]
