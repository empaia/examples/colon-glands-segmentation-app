"""Module to test if the patches are queried correctly"""

from app.utilities.general import get_patch_rectangles

ROI = {"upper_left": (4, 4), "width": 3, "height": 3}
WINDOWS_SIZE = (4, 4)


def test_simple():
    """Test if the patches are queried correctly"""
    upper_lefts = [
        (3, 3),
        (3, 5),
        (5, 3),
        (5, 5),
    ]
    expected = [
        {"upper_left": upper_left, "width": WINDOWS_SIZE[0], "height": WINDOWS_SIZE[1]} for upper_left in upper_lefts
    ]
    actual = get_patch_rectangles(ROI, WINDOWS_SIZE)

    assert expected == actual


def test_odd():
    """Test if the patches are queried correctly if some values are not even but odd."""

    roi = ROI.copy()
    roi["upper_left"] = (4, 5)
    roi["width"] = 4
    upper_lefts = [
        (3, 4),
        (3, 6),
        (5, 4),
        (5, 6),
    ]
    expected = [
        {"upper_left": upper_left, "width": WINDOWS_SIZE[0], "height": WINDOWS_SIZE[1]} for upper_left in upper_lefts
    ]
    actual = get_patch_rectangles(roi, WINDOWS_SIZE)

    assert expected == actual
