"""A module to test the segmentation explanation module."""
import copy

import numpy as np
import pytest
from maxi.data.data_types import EntityRect
from shapely.geometry import Polygon

from app.explanation.get_explanation import (
    get_explanation_polygons,
    get_segmentation_explanation,
    transform_boundaries_to_polygon,
)
from app.tests import DOWN_SAMPLING_FACTOR, IMAGE, REGION_OF_INTEREST, SEGMENTATION_MODEL, UPPER_LEFT


@pytest.mark.filterwarnings("ignore:.invalid value encountered in log")
def test_segmentation_explanation():
    """Test the segmentation explanation module."""
    explanation = get_segmentation_explanation(IMAGE, SEGMENTATION_MODEL, REGION_OF_INTEREST)

    assert explanation.shape == (
        16,
        16,
        3,
    ), f"The explanation has the wrong shape {explanation.shape}, should be (16, 16, 3)"
    assert (explanation != 0).any(), "The explanation only contains zeros. This should not be the case."


def test_segmentation_explanation_not_existing():
    """Test the segmentation explanation module if no segmentation is present."""

    region_of_interest = EntityRect({"x": 32, "y": 32, "w": 16, "h": 16})
    explanations = get_segmentation_explanation(IMAGE, SEGMENTATION_MODEL, region_of_interest)

    np.testing.assert_equal(explanations, 0)


def test_transform_boundaries_to_polygon_simple():
    """Tests if the boundaries are transformed correctly to polygon."""
    boundaries = [(0, 16), (0, 16)]
    expected_polygon = Polygon([(13, 203), (13, 219), (29, 219), (29, 203), (13, 203)])
    actual_polygon = transform_boundaries_to_polygon(boundaries, UPPER_LEFT)
    assert actual_polygon == expected_polygon


def test_transform_boundaries_to_polygon_resolution2():
    """Tests if the boundaries are transformed correctly to polygon if the resolution is 2."""
    resolution = 2
    boundaries = [(0, 8, 16), (0, 8, 16)]
    expected_polygons = [
        [
            Polygon([(13, 203), (13, 211), (21, 211), (21, 203), (13, 203)]),
            Polygon([(13, 211), (13, 219), (21, 219), (21, 211), (13, 211)]),
        ],
        [
            Polygon([(21, 203), (21, 211), (29, 211), (29, 203), (21, 203)]),
            Polygon([(21, 211), (21, 219), (29, 219), (29, 211), (21, 211)]),
        ],
    ]
    for i in range(resolution):
        for j in range(resolution):
            bounds_x = (boundaries[0][i], boundaries[0][i + 1])
            bounds_y = (boundaries[0][j], boundaries[0][j + 1])
            actual_polygon = transform_boundaries_to_polygon([bounds_x, bounds_y], UPPER_LEFT)
            assert actual_polygon == expected_polygons[i][j]


def test_get_segmentation_explanation_polygon():
    """Test the entire workflow: get segmentation explanation and transform it to polygons."""
    upper_left_explanation = (UPPER_LEFT[0] + REGION_OF_INTEREST["x"], UPPER_LEFT[0] + REGION_OF_INTEREST["y"])
    lower_right_explanation = (
        upper_left_explanation[0] + REGION_OF_INTEREST["w"],
        upper_left_explanation[1] + REGION_OF_INTEREST["h"],
    )
    expected_polygon = Polygon(
        [
            upper_left_explanation,
            (upper_left_explanation[0], lower_right_explanation[1]),
            lower_right_explanation,
            (lower_right_explanation[0], upper_left_explanation[1]),
            upper_left_explanation,
        ]
    )

    explanation = get_segmentation_explanation(IMAGE, SEGMENTATION_MODEL, REGION_OF_INTEREST)
    explanation_polygons = get_explanation_polygons(explanation, explanation, upper_left_explanation, resolution=1)

    assert explanation_polygons[0][0]["polygon"] == expected_polygon
    assert explanation_polygons[0][0]["segmentation_value"] > 0


def test_get_segmentation_explanation_polygon_with_downsamplingfactor():
    """Test the entire workflow: get segmentation explanation and transform it to polygons.
    But this time with a downsampling factor unequal to 1."""
    upper_left_explanation = (UPPER_LEFT[0] + REGION_OF_INTEREST["x"], UPPER_LEFT[0] + REGION_OF_INTEREST["y"])
    lower_right_explanation = (
        upper_left_explanation[0] + REGION_OF_INTEREST["w"],
        upper_left_explanation[1] + REGION_OF_INTEREST["h"],
    )
    expected_coordinates = [
        upper_left_explanation,
        (upper_left_explanation[0], lower_right_explanation[1]),
        lower_right_explanation,
        (lower_right_explanation[0], upper_left_explanation[1]),
        upper_left_explanation,
    ]
    expected_polygon = Polygon([(x * DOWN_SAMPLING_FACTOR, y * DOWN_SAMPLING_FACTOR) for x, y in expected_coordinates])

    explanation = get_segmentation_explanation(IMAGE, SEGMENTATION_MODEL, REGION_OF_INTEREST)
    explanation_polygons = get_explanation_polygons(
        explanation, explanation, upper_left_explanation, DOWN_SAMPLING_FACTOR, resolution=1
    )

    assert explanation_polygons[0][0]["polygon"] == expected_polygon
    assert explanation_polygons[0][0]["segmentation_value"] > 0


def test_get_segmentation_explanation_polygon_resolution_2():
    """Test the entire workflow: get segmentation explanation and transform it to polygons.
    But this time with a resolution unequal to 1."""
    resolution = 2

    upper_left_explanation = (UPPER_LEFT[0] + REGION_OF_INTEREST["x"], UPPER_LEFT[0] + REGION_OF_INTEREST["y"])

    explanation = get_segmentation_explanation(IMAGE, SEGMENTATION_MODEL, REGION_OF_INTEREST)
    explanation_polygons = get_explanation_polygons(
        explanation, explanation, upper_left_explanation, resolution=resolution
    )

    assert len(explanation_polygons) == 2
    assert len(explanation_polygons[0]) == 2
    assert len(explanation_polygons[1]) == 2

    upper_left_explanation_00 = copy.deepcopy(upper_left_explanation)
    lower_right_explanation_00 = (
        upper_left_explanation_00[0] + REGION_OF_INTEREST["w"] // resolution,
        upper_left_explanation_00[1] + REGION_OF_INTEREST["h"] // resolution,
    )
    expected_polygon_00 = Polygon(
        [
            upper_left_explanation_00,
            (upper_left_explanation_00[0], lower_right_explanation_00[1]),
            lower_right_explanation_00,
            (lower_right_explanation_00[0], upper_left_explanation_00[1]),
            upper_left_explanation_00,
        ]
    )
    assert explanation_polygons[0][0]["polygon"] == expected_polygon_00
    assert explanation_polygons[0][0]["segmentation_value"] > 0

    upper_left_explanation_11 = copy.deepcopy(lower_right_explanation_00)
    lower_right_explanation_11 = (
        upper_left_explanation_11[0] + REGION_OF_INTEREST["w"] // resolution,
        upper_left_explanation_11[1] + REGION_OF_INTEREST["h"] // resolution,
    )
    expected_polygon_11 = Polygon(
        [
            upper_left_explanation_11,
            (upper_left_explanation_11[0], lower_right_explanation_11[1]),
            lower_right_explanation_11,
            (lower_right_explanation_11[0], upper_left_explanation_11[1]),
            upper_left_explanation_11,
        ]
    )
    assert explanation_polygons[1][1]["polygon"] == expected_polygon_11
    assert explanation_polygons[1][1]["segmentation_value"] > 0
