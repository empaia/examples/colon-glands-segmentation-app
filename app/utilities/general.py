"""Utility functions"""
from math import ceil
from typing import List, Tuple

import numpy as np
import yaml

# pylint: disable=R0914


def batch_splitter(items, batch_size):
    """Split a list into batches of a given size.

    Args:
        items (_type_): _description_
        batch_size (_type_): _description_

    Yields:
        _type_: _description_
    """
    for i in range(0, len(items), batch_size):
        yield items[i : i + batch_size]


def load_config(config_file):
    """
    Loads a yaml config file.
    :param config_file: Path to config file.
    :return: Dictionary of config values.
    """
    with open(config_file, encoding="utf-8") as file:
        return yaml.load(file, Loader=yaml.FullLoader)


def get_patch_rectangles(
    region: dict,
    window: Tuple[int, int],
):
    """Get an iterable of rectangles that represent the patches in a region optionally with intersect.

    Args:
        region (dict): Object describing the region.
        window (Tuple[int, int]): the window size of a patch (width, height)
        downsample_factor (float, optional): Downsample the patches. Defaults to 1.0.
    Returns:
        list: a list of rectangle patches
    """

    window_width, window_height = window
    stride_x = window_width // 2
    stride_y = window_height // 2

    start_x, start_y = region["upper_left"]
    start_x = int(start_x - stride_x)  # x cooridante of the upper left corner of the first patch
    start_y = int(start_y - stride_y)  # y cooridante of the upper left corner of the first patch

    width = region["width"]  # width of the region of interest
    height = region["height"]  # height of the region of interest

    number_of_windows_x = (
        ceil((width - window_width) / stride_x) + 2
    )  # number of patches in x direction; plus 1 to cover full ROI and plus 1 at the start and end
    number_of_windows_y = (
        ceil((height - window_height) / stride_y) + 2
    )  # number of patches in y direction; plus 1 to cover full ROI and plus 1 at the start and end

    result = []

    for i in range(number_of_windows_x):
        for j in range(number_of_windows_y):
            coordinates = (start_x + i * stride_x, start_y + j * stride_y)
            result.append({"upper_left": coordinates, "width": window_width, "height": window_height})

    return result


def normalize_explanation_values(explanation_values: List[float]) -> List[float]:
    """Normalize the explanation values to be between 0 and 1.

    Args:
        explanation_values (List[float]): unnormalized explanation values

    Returns:
        List[float]: normalized explanation values
    """
    # TODO determine if we want to scale the values to be between 0 and 1
    # or if we just want to make sure that they are below 1
    # if np.min(explanation_values) < 0:
    explanation_values_normalized = (explanation_values - np.min(explanation_values)) / (
        np.max(explanation_values) - np.min(explanation_values)
    )
    # else:
    #     explanation_values_normalized = explanation_values / np.max(explanation_values)
    return [float(value) for value in explanation_values_normalized]
