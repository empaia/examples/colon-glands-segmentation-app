"""A module for polygon operations."""
from typing import Any, Dict, List, Tuple, Union

import cv2
import geopandas as gpd
import numpy as np
import shapely
import torch
from shapely.geometry import MultiPolygon, Polygon

# pylint: disable=R0914


def find_ploygons(mask, offset, upscale_factor):
    """
    Finds polygons in a single-channel image. Treats any non-zero value as True, and zeros as False.

    :param binary_mask_image: A single-channel image.
    :return: A list of polygons.
    """
    idx = mask > 0.5
    mask[idx] = 255
    mask[~idx] = 0
    mask = mask.astype(np.uint8)
    contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE, offset=offset)

    return [Polygon(contour.squeeze() * upscale_factor) for contour in contours if len(contour) > 2]


def merge_interesecting_polygons(
    polygons: List[Polygon], predictions: List[np.ndarray]
) -> Tuple[List[Union[Polygon, MultiPolygon]], List[List[np.ndarray]], List[List[Polygon]], List[List[List[float]]]]:
    """Merges intersecting polygons, converting them to shapely polygons if they are not already.
    The prediction scores of the merged Polygon are determined by
      1. the class prediction probability for each polygon
      2. weighted by the area of the polygon.
    The function also returns the polygons and their prediction scores before merging assigned to the merged polygons.

    Args:
        polygons (List[Polygon]): polygons to be merged of they are intersecting
        predictions (List[np.ndarray]): prediction probability for all classes of each polygon

    Returns:
        Tuple[List[Union[Polygon, MultiPolygon]], List[List[np.ndarray]], List[List[Polygon]], List[List[List[float]]]]:
            Merged polygons, their class prediction scores,
            their original polygons and their original prediction scores
    """

    gdf = gpd.GeoDataFrame({"geometry": polygons, "predictions": predictions})

    # self join on geodataframe to get all polygon intersections
    intersects = gdf.sjoin(gdf[["geometry"]], how="left", predicate="intersects").reset_index()

    # dissolve intersections on right index indices aggregating to list
    intersects_diss = intersects.dissolve(
        "index_right",
        aggfunc=lambda x: x.tolist(),
    )
    intersects_diss["predictions"] = gdf["predictions"]
    intersects_diss.geometry = intersects_diss.geometry.buffer(0)  # validate geometries
    intersects_diss["area"] = intersects_diss.geometry.area  # area of polygons

    # determine which polygons are connected
    intersects_diss["polygon_group"] = None
    polygons_assigned, predictions_assigned = [], []
    j = 0
    for i, polygon in enumerate(polygons):
        if intersects_diss.loc[i, "polygon_group"] is None:
            intersects_diss.loc[i, "polygon_group"] = j
            polygons_assigned.append([polygon])
            predictions_assigned.append([predictions[i].tolist()])
            j += 1
        else:
            polygons_assigned[intersects_diss.loc[i, "polygon_group"]].append(polygon)
            predictions_assigned[intersects_diss.loc[i, "polygon_group"]].append(predictions[i].tolist())
        poly_group = intersects_diss.loc[i, "polygon_group"]
        intersects_diss.loc[intersects_diss["index"].apply(lambda x: i in x), "polygon_group"] = poly_group

    # dissolve again on left index using mode
    polygons_merged = intersects_diss.dissolve(
        "polygon_group",
        aggfunc=lambda x: x.tolist(),
    )
    polygons_merged.geometry = polygons_merged.geometry.buffer(0)  # validate geometries
    # remove empty geometries
    polygons_merged = polygons_merged[~polygons_merged.geometry.is_empty]

    # for each merged polygon determine the label by:
    #   1. covered area
    #   2. prediction probability
    number_of_classes = list(range(predictions[0].shape[0]))
    polygons_merged[number_of_classes] = 0
    for index, row in polygons_merged.iterrows():
        for preds, area in zip(row["predictions"], row["area"]):
            for cls, prob in enumerate(preds):
                polygons_merged.loc[index, cls] += area * prob  # area times probability

    predictions_summed = polygons_merged.loc[:, number_of_classes].sum(axis=1)
    polygons_merged.loc[:, number_of_classes] = polygons_merged.loc[:, number_of_classes].divide(
        predictions_summed, axis=0
    )

    return (
        list(polygons_merged["geometry"]),
        polygons_merged[number_of_classes].to_numpy().tolist(),
        polygons_assigned,
        predictions_assigned,
    )


def limit_poly_to_roi(
    roi: Dict, polygons: List[Union[Polygon, MultiPolygon]], polygon_values: List[List[Any]]
) -> Tuple[List[List[Tuple[int, int]]], List[Any]]:
    """Limits the polygons to only lay within the Region of Interest. Assigns the values of the polygons to the polygons

    Args:
        roi (Dict): Region of Interest
        polygons (List[Union[Polygon, MultiPolygon]]): List of polygons
        polygon_values (List[List[Any]]): List of values for each polygon,
                        e.g. [prediction_score_per_polygon: List[float], area_per_polygon: List[float], ...]

    Returns:
        Tuple[List[List[Tuple[int, int]]], List[Any]]: List of roi-limited polygons coordinates and their values
    """
    upper_left = roi["upper_left"]
    width, height = roi["width"], roi["height"]
    roi_polygon = Polygon(
        [
            upper_left,
            (upper_left[0] + width, upper_left[1]),
            (upper_left[0] + width, upper_left[1] + height),
            (upper_left[0], upper_left[1] + height),
            upper_left,
        ]
    )

    poly_corrdinates, values = [], [[] for _ in range(len(polygon_values))]
    for elements in zip(polygons, *polygon_values):
        polygon = shapely.make_valid(elements[0])
        if not shapely.contains(roi_polygon, polygon):
            polygon = shapely.make_valid(shapely.intersection(roi_polygon, polygon))
            if polygon.is_empty or not isinstance(polygon, (MultiPolygon, Polygon)):
                continue
        coordinates = transform_poly_to_coords(polygon)
        poly_corrdinates.extend(coordinates)
        for i, element in enumerate(elements[1:]):
            values[i].extend([element] * len(coordinates))
    return poly_corrdinates, values


def transform_poly_to_coords(polygon: Union[Polygon, MultiPolygon]) -> List[List[Tuple[int, int]]]:
    """Transforms a Polygon into a List of coordinates

    Args:
        polygons (Union[Polygon, MultiPolygon]): a single Polygon

    Returns:
        List[List[Tuple[int, int]]]: List of List of coordinates
    """
    if isinstance(polygon, MultiPolygon):
        coordinates = []
        for poly in list(polygon.geoms):
            coordinates.append([(int(x), int(y)) for x, y in poly.exterior.coords])
        return coordinates
    if not isinstance(polygon, Polygon):
        raise TypeError(f"Polygon type {type(polygon)} not supported")
    return [[(int(x), int(y)) for x, y in polygon.exterior.coords]]


def upsample_explanation(tile_size, segmentation_explanation):
    """Upsamples the explanation to the tile size."""
    segmentation_explanation_torch = torch.Tensor(segmentation_explanation).moveaxis(-1, 0).unsqueeze(0)
    segmentation_explanation_upsampled = torch.nn.functional.interpolate(
        segmentation_explanation_torch,
        size=(tile_size, tile_size),
        mode="bilinear",
        align_corners=False,
    )
    segmentation_explanation_upsampled = (
        segmentation_explanation_upsampled.squeeze(0).moveaxis(0, -1).cpu().detach().numpy()
    )

    return segmentation_explanation_upsampled
