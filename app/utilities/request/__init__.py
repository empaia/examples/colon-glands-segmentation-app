"""Module which contains requests constants"""
from app.constants import TOKEN

TIMEOUT = 60
HEADERS = {"Authorization": f"Bearer {TOKEN}"}
