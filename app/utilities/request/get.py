"""Module for all getter functions"""
from io import BytesIO

import requests
from PIL import Image

from app.constants import APP_API, JOB_ID
from app.utilities.request import HEADERS, TIMEOUT

# pylint: disable=C0103


def get_input(key: str):
    """
    get input data by key as defined in EAD
    """
    url = f"{APP_API}/v3/{JOB_ID}/inputs/{key}"
    response = requests.get(url, headers=HEADERS, timeout=TIMEOUT)
    if response.status_code != 200:
        raise ConnectionError(response.content)
    return response.json()


async def get_wsi_tile(wsi: dict, rectangle: dict, level: int, feature_extractor):
    """
    get a WSI tile from given level and rectangle

    Parameters:
        my_wsi: contains WSI id (and meta data)
        my_rectangle: tile position on level 0
    """
    x, y = rectangle["upper_left"]
    width = rectangle["width"]
    height = rectangle["height"]

    wsi_id = wsi["id"]

    tile_url = f"{APP_API}/v3/{JOB_ID}/regions/{wsi_id}/level/{level}/start/{x}/{y}/size/{width}/{height}"
    response = requests.get(tile_url, headers=HEADERS, timeout=TIMEOUT)
    response.raise_for_status()
    image = Image.open(BytesIO(response.content))
    image_tensor = feature_extractor(image, return_tensors="pt")["pixel_values"].squeeze()
    return image_tensor
