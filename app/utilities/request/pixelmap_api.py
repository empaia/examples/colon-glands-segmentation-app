""" Pixelmap API requests """

import requests

from app.constants import APP_API, JOB_ID, logger
from app.utilities.request import HEADERS, TIMEOUT

# PIXELMAP_BASE_URL = f"{APP_API}/v3"
PIXELMAP_BASE_URL = f"{APP_API}/v3/{JOB_ID}/pixelmaps"

PIXELMAP = {
    "type": "continuous_pixelmap",
    "element_type": "float32",
    "channel_count": 2,
    "reference_type": "wsi",
    "creator_type": "job",
    "min_value": 0.0,
    "max_value": 1.0,
    "creator_id": JOB_ID,
    "tilesize": None,
    "name": None,
    "reference_id": None,
    "levels": None,
    "description": None,
}


def put_tile(pixelmap_id, level, tile_x, tile_y, data):
    """
    Put tile to the pixelmap server

    Args:
        pixelmap_id (str): id of the pixelmap
        level (int): pixelmap level
        tile_x (int): x coordinate of the tile
        tile_y (int): y coordinate of the tile
        file (file): file to put
    """
    response = requests.put(
        f"{PIXELMAP_BASE_URL}/{pixelmap_id}/level/{level}/position/{tile_x}/{tile_y}/data",
        data=data,
        headers=HEADERS,
        timeout=TIMEOUT,
    )
    if response.status_code != 204:
        logger.error("\033[91mError creating pixelmap: %s\033[0m", response.text)
        response.raise_for_status()
    return response
