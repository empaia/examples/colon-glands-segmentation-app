"""Module for all post functions"""
import copy
from typing import Any, Dict, List, Tuple, Union

import requests

from app.constants import APP_API, JOB_ID, OUTPUT_POSTING_BATCH_SIZE, logger
from app.utilities.request import HEADERS, TIMEOUT

# pylint: disable=R0913, R0914, R0915
JOB_REFERENCE = {
    "creator_type": "job",
    "creator_id": JOB_ID,
}
EMPTY_VALUE = copy.deepcopy(JOB_REFERENCE)
EMPTY_VALUE["reference_type"] = "annotation"

EMPTY_COLLECTION: dict[str, Any] = copy.deepcopy(JOB_REFERENCE)
EMPTY_COLLECTION["type"] = "collection"
EMPTY_COLLECTION["items"] = []

EMPTY_FLOAT_COLLECTION = copy.deepcopy(EMPTY_COLLECTION)
EMPTY_FLOAT_COLLECTION["item_type"] = "float"
EMPTY_FLOAT_COLLECTION["reference_type"] = "annotation"

EMPTY_CLASS_COLLECTION = copy.deepcopy(EMPTY_COLLECTION)
EMPTY_CLASS_COLLECTION["item_type"] = "class"

EMPTY_COLLECTION_COLLECTION = copy.deepcopy(EMPTY_COLLECTION)
EMPTY_COLLECTION_COLLECTION["item_type"] = "collection"

EMPTY_ANNOTATION_COLLECTION = copy.deepcopy(EMPTY_COLLECTION)
EMPTY_ANNOTATION_COLLECTION["item_type"] = "polygon"
EMPTY_ANNOTATION_COLLECTION["reference_type"] = "annotation"


def post_segmentations(
    wsi: Dict,
    roi: Dict,
    segmentation_coordinates: List[List[Tuple[int, int]]],
    segmentation_labels: List[str],
    segmentation_predictions: List[List[float]],
    segmentation_area: List[float],
    segmentation_predictions_detailed: List[List[List[float]]],
    segmentation_area_detailed: List[List[float]],
    index2class: Dict[int, str],
) -> None:
    """Posts the segmentations and its values (labels, predictions, area)

    Args:
        wsi (Dict): information on the WSI
        roi (Dict): information on the Region of Interest
        segmentation_coordinates (List[List[Tuple[int, int]]]): List of coordinates for each segmentation
        segmentation_labels (List[str]): class labels for each segmentation
        segmentation_predictions (List[List[float]]): class prediction scores for each segmentation
        segmentation_area (List[float]): segmentation area in nanometer squared
        segmentation_predictions_detailed (List[List[List[float]]]): prediction scores for each unmerged segmentation
        segmentation_area_detailed (List[List[float]]): area of each unmerged segmentation
        index2class (Dict[int, str]): mapping from class index to class name
    """
    empty_collection = copy.deepcopy(EMPTY_ANNOTATION_COLLECTION)
    empty_collection["reference_id"] = roi["id"]
    logger.info("Posting polygons to the server")
    collection = post_output("segmentation", empty_collection)

    polygon_collection = [
        {
            "name": "Segmentation annotation",
            "type": "polygon",
            "reference_id": wsi["id"],
            "reference_type": "wsi",
            # Always use WSI base level coordinates
            "coordinates": coords,
            "npp_created": roi["npp_created"],
            "npp_viewing": [
                200,
                roi["npp_created"],
            ],
            "creator_type": "job",  # NEW required in v3 apps
            "creator_id": JOB_ID,  # NEW required in v3 apps
        }
        for coords in segmentation_coordinates
    ]
    items: List[Dict] = post_many_items_to_collection(collection["id"], polygon_collection)
    logger.info("Number of items in collection %s", len(items))

    logger.info("Posting segmentation class, prediction scores and area to the server")
    class_collection = post_output("segmentation_classification", EMPTY_CLASS_COLLECTION)
    info_collection = post_output("segmentation_info", EMPTY_COLLECTION_COLLECTION)
    info_detailed_collection = post_output("segmentation_info_detailed", EMPTY_COLLECTION_COLLECTION)

    labels, infos, infos_detailed = [], [], []
    for label, predictions, area, preds_detailed, areas_detailed, item in zip(
        segmentation_labels,
        segmentation_predictions,
        segmentation_area,
        segmentation_predictions_detailed,
        segmentation_area_detailed,
        items,
    ):
        # add segmentation label
        label_value = copy.deepcopy(EMPTY_VALUE)
        label_value["value"] = label
        label_value["reference_id"] = item["id"]
        label_value["type"] = "class"
        labels.append(label_value)

        # add segmentation info
        info: Dict[str, Any] = copy.deepcopy(EMPTY_FLOAT_COLLECTION)
        info["reference_id"] = item["id"]
        info["reference_type"] = "annotation"
        # add segmentation prediction scores
        for i, prediction in enumerate(predictions):
            prediction_info: Dict[str, Any] = copy.deepcopy(EMPTY_VALUE)
            prediction_info["value"] = prediction
            prediction_info["name"] = f"Prediction score for class '{index2class[i]}'"
            prediction_info["description"] = "For the merged segmentation"
            prediction_info["reference_id"] = item["id"]
            prediction_info["reference_type"] = "annotation"
            prediction_info["type"] = "float"
            info["items"].append(prediction_info)
        # add segmentation area
        area_info: Dict[str, Any] = copy.deepcopy(EMPTY_VALUE)
        area_info["value"] = area
        area_info["name"] = "Segmentation area"
        area_info["description"] = "For the merged segmentation"
        area_info["reference_id"] = item["id"]
        area_info["type"] = "float"
        info["items"].append(area_info)
        infos.append(info)

        # add detailed (unmerged) segmentation info
        info_detailed: Dict[str, Any] = copy.deepcopy(EMPTY_FLOAT_COLLECTION)
        info_detailed["reference_id"] = item["id"]
        info_detailed["reference_type"] = "annotation"
        # add segmentation prediction scores
        for j, predictions in enumerate(preds_detailed):
            for i, prediction in enumerate(predictions):
                prediction_info: Dict[str, Any] = copy.deepcopy(EMPTY_VALUE)
                prediction_info["value"] = prediction
                prediction_info["name"] = f"Prediction score for class '{index2class[i]}'"
                prediction_info["description"] = f"For the polygon {j}"
                prediction_info["reference_id"] = item["id"]
                prediction_info["reference_type"] = "annotation"
                prediction_info["type"] = "float"
                info_detailed["items"].append(prediction_info)
        # add segmentation area
        for j, area in enumerate(areas_detailed):
            area_info: Dict[str, Any] = copy.deepcopy(EMPTY_VALUE)
            area_info["value"] = area
            area_info["name"] = "Segmentation area"
            area_info["description"] = f"For the polygon {j}"
            area_info["reference_id"] = item["id"]
            area_info["type"] = "float"
            info_detailed["items"].append(area_info)
        infos_detailed.append(info_detailed)

    post_many_items_to_collection(class_collection["id"], labels)
    post_many_collections_to_collection(info_collection["id"], infos)
    post_many_collections_to_collection(info_detailed_collection["id"], infos_detailed)


def post_output(key: str, data: dict):
    """
    post output data by key as defined in EAD

    :param key: key to post data under
    :param data: data to post

    :return: updated output
    """
    url = f"{APP_API}/v3/{JOB_ID}/outputs/{key}"
    response = requests.post(url, json=data, headers=HEADERS, timeout=TIMEOUT)
    if response.status_code != 200:
        raise ConnectionError(response.content)
    return response.json()


def post_many_items_to_collection(collection_id: int, items: List[Dict[str, Union[str, float]]]) -> List:
    """Posts many items to a collection

    Args:
        collection (int): collection to which to add new items
        predictions (List[Dict[str, Union[str, float]]]): list of items to add
    Returns:
        List: item ids of the polygons
    """
    start_idx = 0
    returned_items: List[Dict] = []

    while start_idx < len(items):
        collection = post_items_to_collection(collection_id, items[start_idx : start_idx + OUTPUT_POSTING_BATCH_SIZE])
        start_idx += OUTPUT_POSTING_BATCH_SIZE
        returned_items.extend(collection["items"])
    return returned_items


def post_many_collections_to_collection(collection_id: int, items: List[Dict[str, Union[List[Any], Any]]]) -> List:
    """Posts many collections to a collection (e.g. segmentation info)

    Args:
        collection (int): collection to which to add new items
        predictions (List[Dict[str, List | Any]]): list of items to add
    Returns:
        List: item ids of the polygons
    """
    returned_items: List[Dict] = []
    items = sorted(items, key=lambda x: len(x["items"]))

    # let's deal with collection with number of items larger than OUTPUT_POSTING_BATCH_SIZE
    while len(items) > 0 and len(items[0]["items"]) > OUTPUT_POSTING_BATCH_SIZE:
        logger.info("Very large collection encountered, posting bit by bit")
        large_item = items.pop()
        empty_item = copy.deepcopy(large_item)
        empty_item["items"] = []
        collection = post_items_to_collection(collection_id, [empty_item])
        # find the empty collection
        large_collection_id = None
        for item in collection["items"]:
            if not item["items"]:
                large_collection_id = item["id"]
        if large_collection_id is None:
            raise ValueError("Something went terribly wrong")
        post_many_items_to_collection(large_collection_id, large_item["items"])

    start_idx, end_idx = 0, 0
    number_of_items = 0

    while start_idx < len(items):
        while len(items[end_idx]["items"]) + number_of_items > OUTPUT_POSTING_BATCH_SIZE and end_idx < len(items):
            end_idx += 1
            number_of_items += len(items[end_idx]["items"])
        collection = post_items_to_collection(collection_id, items[start_idx : start_idx + end_idx + 1])
        start_idx += end_idx + 1
        returned_items.extend(collection["items"])
    return returned_items


def post_items_to_collection(collection_id: int, items: list[Dict[str, Any]]) -> dict:
    """
    Add items to an existing output collection

    Args:
        collection_id (int): collection to which to add new items
        items (list[Dict[str, Any]]): list of items to add
    Returns:
        dict: updated collection
    """
    url = f"{APP_API}/v3/{JOB_ID}/collections/{collection_id}/items"
    response = requests.post(url, json={"items": items}, headers=HEADERS, timeout=TIMEOUT)
    response.raise_for_status()
    items = response.json()
    return items
