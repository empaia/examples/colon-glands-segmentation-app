"""Module for all putter functions"""

import requests

from app.constants import APP_API, JOB_ID
from app.utilities.request import HEADERS, TIMEOUT


def put_finalize():
    """Finalize job, such that no more data can be added
    and to inform EMPAIA infrastructure about job state"""
    url = f"{APP_API}/v3/{JOB_ID}/finalize"
    response = requests.put(url, headers=HEADERS, timeout=TIMEOUT)
    response.raise_for_status()


# TODO: max 255 message length
def put_failure(msg: str):
    """
    post error text to the API /failure endpoint as feedback to user

    Parameters:
        error: either an exception or simple string to inform the user of
        why the app failed
    """
    url = f"{APP_API}/v3/{JOB_ID}/failure"

    response = requests.put(url, json={"user_message": msg}, headers=HEADERS, timeout=TIMEOUT)
    response.raise_for_status()
