#!/bin/bash
printf "[ ! ] Starting the docker build process \n"
docker build -t colon-glands .
printf "[ \xE2\x9C\x94 ] Docker build process completed \n"
printf "[ ! ] Registering the App with eats \n"
eats apps register ./ead.json colon-glands > app.env 
printf "[ \xE2\x9C\x94 ] App registered successfully \n"
printf "[ ! ] Registering the slides with eats \n"
eats slides register ./inputs/my_wsi.json
printf "[ \xE2\x9C\x94 ] Slides registered successfully \n"
export $(xargs < app.env) 
echo $APP_ID



